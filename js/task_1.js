
export const TASK_1 = () => {

    const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
    const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

    const [...clients] = new Set([...clients1, ...clients2]);

    // let clients = [...clients1, ...clients2];
    // clients = clients.filter((el, i) => clients.indexOf(el) === i);

    console.log('clients', clients);
}