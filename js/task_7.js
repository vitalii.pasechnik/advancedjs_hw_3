
export const TASK_7 = () => {

    const array = ['value', () => 'showValue'];

    const [value, showValue] = array;

    console.log('value =>', value); // має бути виведено 'value'
    console.log('showValue() =>', showValue());  // має бути виведено 'showValue'
}