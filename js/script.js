import { TASK_1 } from "./task_1.js";
import { TASK_2 } from "./task_2.js";
import { TASK_3 } from "./task_3.js";
import { TASK_4 } from "./task_4.js";
import { TASK_5 } from "./task_5.js";
import { TASK_6 } from "./task_6.js";
import { TASK_7 } from "./task_7.js";

console.log("----- TASK 1 -----");
TASK_1();

console.log("----- TASK 2 -----");
TASK_2();

console.log("----- TASK 3 -----");
TASK_3();

console.log("----- TASK 4 -----");
TASK_4();

console.log("----- TASK 5 -----");
TASK_5();

console.log("----- TASK 6 -----");
TASK_6();

console.log("----- TASK 7 -----");
TASK_7();

// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна

// Деструктуризация - это процесс разбития сложной структуры на более простые. 
// А именно, с помощью деструктуризации, можно вытягивать из объектов или массивов нужные нам поля, не перебирая весь объект или массив.
// Также, с помощью деструктуризации можно объединять несколько массивов или объектов в один, делать неглубокое клонирование объектов  