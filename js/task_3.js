
export const TASK_3 = () => {

    const user1 = {
        name: "John",
        years: 30,
    };

    const { name, years: age, isAdmin = false } = user1;

    const logOut = () => {
        console.log('name', name);
        console.log('age', age);
        console.log('isAdmin', isAdmin);
    }
    logOut();
}