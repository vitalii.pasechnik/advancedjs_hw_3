
export const TASK_2 = () => {
    
    const characters = [
        {
            name: "Елена",
            lastName: "Гилберт",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Кэролайн",
            lastName: "Форбс",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Аларик",
            lastName: "Зальцман",
            age: 31,
            gender: "man",
            status: "human"
        },
        {
            name: "Дэймон",
            lastName: "Сальваторе",
            age: 156,
            gender: "man",
            status: "vampire"
        },
        {
            name: "Ребекка",
            lastName: "Майклсон",
            age: 1089,
            gender: "woman",
            status: "vempire"
        },
        {
            name: "Клаус",
            lastName: "Майклсон",
            age: 1093,
            gender: "man",
            status: "vampire"
        }
    ];

    let charactersShortInfo = characters.map(obj => {
        const { name, lastName, age } = obj;
        return { name, lastName, age }
    });

    // const charactersShortInfo = characters.map(({ gender, status, ...rest }) => rest );
    console.log("charactersShortInfo", charactersShortInfo);
}