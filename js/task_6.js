
export const TASK_6 = () => {

    const employee = {
        name: 'Vitalii',
        surname: 'Klichko'
    }

    const newEmployee = {
        ...employee,
        age: 45,
        salary: '1000000$'
    }

    console.log('employee', employee);
    console.log('newEmployee', newEmployee);
}